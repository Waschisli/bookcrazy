package Apresentacao;

import Entidades.Cliente;
import Entidades.Funcionario;
import Entidades.Livro;
import Negocio.NegocioCliente;
import Negocio.NegocioUtils;
import java.util.ArrayList;
import java.util.Scanner;

public class ApresentacaoCliente {
	
	//Objetos necess�rios para manipula��o na classe.
	Scanner input = new Scanner(System.in);
	public static NegocioCliente negocioCliente = new NegocioCliente();
	public static NegocioUtils negocioUtils = new NegocioUtils();
	
	/**
	 * M�todo que coleta as informa��es para cadastro do Cliente e chama o m�todo cadastrarCliente da classe NegocioCliente.
	 * @return
	 */
	public boolean cadastrarCliente() {
		System.out.println("Qual o nome do Cliente: ");
		String nome = input.nextLine();
		System.out.println("Qual o sobrenome do Cliente: ");
		String sobrenome = input.nextLine();
		System.out.println("Qual o cpf do Cliente: ");
		String cpf = input.nextLine();
		System.out.println("Qual o endere�o do Cliente: ");
		String endereco = input.nextLine();
		NegocioCliente negocioClienteCadastrar = new NegocioCliente(nome,sobrenome,cpf,endereco);
		return negocioClienteCadastrar.cadastrarCliente(negocioClienteCadastrar);

	}
	
	/**
	 * M�todo que chama o m�todos listarCliente da classe NegocioCliente e mostra todos os Clientes.
	 */
	public void listarCliente() {
		ArrayList<Cliente> listarCliente = negocioCliente.listarCliente();
		System.out.println("                                           ");
		System.out.println("==========Relat�rio de clientes============");
		for (int i = 0; i < listarCliente.size(); i++) {
			System.out.println(i + " - " + listarCliente.get(i).toString());
		}
		System.out.println("                                           ");
	}
	
	//M�todo que captura a posi��o do Cliente a ser removido e chama o m�todo removerCliente da classe NegocioCliente.
	public boolean removerCliente() {
		System.out.println("Digite a posi��o que voc� quer excluir: ");
		int remover = input.nextInt();
		NegocioCliente negocioClienteRemover = new NegocioCliente();
		return negocioClienteRemover.removerCliente(remover);
	}
	
	/**
	 * M�todo que captura a posi��o do Cliente a ser removido e as informa��es a serem alteradas, ap�s isso chama o m�todo
	 * atualizarCliente da classe NegocioCliente.
	 */
	public boolean atualizarCliente() {
		System.out.println("Qual a posi��o do cliente que deseja alterar? ");
		int alterar = Integer.parseInt(input.nextLine());
		System.out.println("Qual o nome do cliente: ");
		String nome = input.nextLine();
		System.out.println("Qual o sobrenome do cliente: ");
		String sobrenome = input.nextLine();
		System.out.println("Qual o cpf do cliente: ");
		String cpf = input.nextLine();
		System.out.println("Qual o endere�o do cliente: ");
		String endereco = input.nextLine();
		NegocioCliente negocioClienteAtualizar = new NegocioCliente(nome,sobrenome,cpf,endereco);
		return negocioClienteAtualizar.atualizarCliente(alterar,negocioClienteAtualizar);
	}
}
