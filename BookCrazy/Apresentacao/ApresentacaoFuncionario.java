package Apresentacao;

import Entidades.Cliente;
import Entidades.Funcionario;
import Negocio.NegocioFuncionario;
import Negocio.NegocioUtils;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author matheus, J�lio Oliveira.
 */
public class ApresentacaoFuncionario {
	
	//Objetos necess�rios para manipula��o na classe.
	Scanner input = new Scanner(System.in);
	public static NegocioFuncionario negocioFuncionario = new NegocioFuncionario();
	public static NegocioUtils negocioUtils = new NegocioUtils();
	
	/**
	 * M�todo que coleta as informa��es para cadastro do funcion�rio e chama o m�todo cadastrarFuncionario da classe NegocioFuncionario.
	 * @return
	 */
	public boolean cadastrarFuncionario() {
		System.out.println("Qual o setor do fucion�rio?");
		String setor = input.nextLine();
		System.out.println("Qual a matricula do funcion�rio?");
		int matricula = Integer.parseInt(input.nextLine());
		System.out.println("Qual o nome do Funcionario: ");
		String nome = input.nextLine();
		System.out.println("Qual o sobrenome do Funcionario: ");
		String sobrenome = input.nextLine();
		System.out.println("Qual o cpf do Funcionario: ");
		String cpf = input.nextLine();
		System.out.println("Qual o endere�o do Funcionario: ");
		String endereco = input.nextLine();
		NegocioFuncionario negocioFuncionarioCadastrar = new NegocioFuncionario(setor,matricula,nome,sobrenome,cpf,endereco);
		return negocioFuncionarioCadastrar.cadastrarFuncionario(negocioFuncionarioCadastrar);

	}
	
	/**
	 * M�todo que chama o m�todo listarFuncionario da classe NegocioFuncionario e mostra todos os funcion�rios.
	 */
	public void listarFuncionario() {
		ArrayList<Funcionario> listarFuncionario = negocioFuncionario.listarFuncionario();
		System.out.println("                                           ");
		System.out.println("==========Relat�rio de funcion�rios============");
		for (int i = 0; i < listarFuncionario.size(); i++) {
			negocioUtils.formatarCpf(listarFuncionario.get(i).getCpf());
			System.out.println(i + " - " + listarFuncionario.get(i).toString());
		}
		System.out.println("                                           ");
	}
	
	//M�todo que captura a posi��o do funcion�rio a ser removido e chama o m�todo removerFuncionario da classe NegocioFuncionario.
	public boolean removerFuncionario() {
		System.out.println("Digite a posi��o que voc� quer excluir: ");
		int remover = input.nextInt();
		NegocioFuncionario negocioFuncionarioRemover = new NegocioFuncionario();
		return negocioFuncionarioRemover.removerFuncionario(remover);
	}
	
	/**
	 * M�todo que captura a posi��o do funcion�rio a ser removido e as informa��es a serem alteradas, ap�s isso chama o m�todo
	 * atualizarFuncionario da classe NegocioFuncionario.
	 */
	public boolean atualizarFuncionario() {
		System.out.println("Qual a posi��o do funcion�rio que deseja alterar? ");
		int alterar = Integer.parseInt(input.nextLine());
		System.out.println("Qual o setor do fucion�rio?");
		String setor = input.nextLine();
		System.out.println("Qual a matricula do funcion�rio?");
		int matricula = Integer.parseInt(input.nextLine());
		System.out.println("Qual o nome do Funcionario: ");
		String nome = input.nextLine();
		System.out.println("Qual o sobrenome do Funcionario: ");
		String sobrenome = input.nextLine();
		System.out.println("Qual o cpf do Funcionario: ");
		String cpf = input.nextLine();
		System.out.println("Qual o endere�o do Funcionario: ");
		String endereco = input.nextLine();
		NegocioFuncionario negocioFuncionarioAtualizar = new NegocioFuncionario(setor,matricula,nome,sobrenome,cpf,endereco);
		return negocioFuncionarioAtualizar.atualizarFuncionario(alterar,negocioFuncionarioAtualizar);
	}

}
