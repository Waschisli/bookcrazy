
package Apresentacao;

import Entidades.Cliente;
import Entidades.Livro;
import Negocio.NegocioLivro;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author matheus,waschislin,julio cesar
 */
public class ApresentacaoLivro { 
	
	//Objetos necess�rios para manipula��o na classe.
	public static NegocioLivro negocioLivro = new NegocioLivro();
	Scanner input = new Scanner(System.in);

	/**
	 * M�todo que coleta as informa��es para cadastro do Livro e chama o m�todo cadastrarLivro da classe NegocioLivro.
	 * @return
	 */
	public boolean cadastrarLivro() {
		System.out.println("Digite o nome do Livro: ");
		String nomeLivro = input.nextLine();
		System.out.println("Digite o autor do Livro: ");
		String autor = input.nextLine();
		System.out.println("Digite o ISBN do Livro: ");
		Long isbn = Long.parseLong(input.nextLine());
		System.out.println("Digite a editora do Livro: ");
		String editora = input.nextLine();
		System.out.println("Digite o n�mero de p�ginas do livro: ");
		int numPag = Integer.parseInt(input.nextLine());
		NegocioLivro negocioLivroCadastrar = new NegocioLivro(nomeLivro,autor,isbn,editora,numPag);
		return negocioLivroCadastrar.cadastrarLivro(negocioLivroCadastrar);
	}
	
	/**
	 * M�todo que chama o m�todos listarLivro da classe NegocioLivro e mostra todos os Livros.
	 */
	public void listarLivros() {
		ArrayList<Livro> listarLivros = negocioLivro.listarLivros();
		System.out.println("                                           ");
		System.out.println("==========Relat�rio de clientes============");
		for (int i = 0; i < listarLivros.size(); i++) {
			System.out.println(i + " - " + listarLivros.get(i).toString());
		}
		System.out.println("                                           ");
	}
	
	//M�todo que captura a posi��o do Livro a ser removido e chama o m�todo removerLivro da classe NegocioLivro.
	public boolean removerLivro() {
		System.out.println("Digite a posi��o do livro que voc� quer excluir: ");
		int remover = input.nextInt();
		NegocioLivro negocioLivroRemover = new NegocioLivro();
		return negocioLivroRemover.removerLivro(remover);
	}
	
	/**
	 * M�todo que captura a posi��o do Livro a ser removido e as informa��es a serem alteradas, ap�s isso chama o m�todo
	 * atualizarLivro da classe NegocioLivro.
	 */
	public boolean atualizarLivro() {
		System.out.println("Qual a posi��o do livro que voc� deseja editar? ");
		int alterar = Integer.parseInt(input.nextLine());
		System.out.println("Digite o nome do Livro: ");
		String nomeLivro = input.nextLine();
		System.out.println("Digite o autor do Livro: ");
		String autor = input.nextLine();
		System.out.println("Digite o ISBN do Livro: ");
		Long isbn = Long.parseLong(input.nextLine());
		System.out.println("Digite a editora do Livro: ");
		String editora = input.nextLine();
		System.out.println("Digite o n�mero de p�ginas do livro: ");
		int numPag = Integer.parseInt(input.nextLine());
		NegocioLivro negocioLivroAtualizar = new NegocioLivro(nomeLivro,autor,isbn,editora,numPag);
		return negocioLivroAtualizar.alterarLivro(alterar,negocioLivroAtualizar);
	}
}
