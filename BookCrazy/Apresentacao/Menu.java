/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Apresentacao;

import java.util.ArrayList;
import java.util.Scanner;
import Negocio.NegocioCliente;
import Negocio.NegocioLivro;
import Negocio.NegocioFuncionario;

/**
 *
 * @author matheus,J�lio,Waschislin.
 */
public class Menu {

	ApresentacaoFuncionario apresentacaoFuncionario = new ApresentacaoFuncionario();
	ApresentacaoCliente apresentacaoCliente = new ApresentacaoCliente();
	NegocioLivro negocioLivro = new NegocioLivro();
	NegocioFuncionario negocioFuncionario = new NegocioFuncionario();
	ApresentacaoLivro apresentacaoLivro = new ApresentacaoLivro();

	public void Menu() {

		Scanner input = new Scanner(System.in);
		boolean menu = true;
		while (menu == true) {
			System.out.println("======Bem Vindo � BookCrazy======");
			System.out.println("Escolha uma op��o: ");
			System.out.println("1 - Menu de Funcion�rios.");
			System.out.println("2 - Menu de Clientes.");
			System.out.println("3 - Menu de Livros.");
			System.out.println("4 - Sair do sistema.");
			int escolhaMenu = input.nextInt();
			switch (escolhaMenu) {
			case 1:
				System.out.println("======Menu de Funcion�rios=======");
				System.out.println("Escolha uma op��o: ");
				System.out.println("1 - Cadastrar funcion�rio.");
				System.out.println("2 - Alterar o cadastro de um funcion�rio.");
				System.out.println("3 - Listar todos os funcion�rios.");
				System.out.println("4 - Excluir um funcion�rio.");
				System.out.println("5 - Sair do sistema.");
				int escolhaMenuFuncionarios = input.nextInt();
				switch (escolhaMenuFuncionarios) {
				case 1:
					if (apresentacaoFuncionario.cadastrarFuncionario()) {
						System.out.println("Salvo com sucesso.");
						break;
					}
					System.out.println("N�o salvo.");
					break;
				case 2:
					if (apresentacaoFuncionario.atualizarFuncionario()) {
						System.out.println("Salvo com sucesso.");
						break;
					}
					System.out.println("N�o salvo.");
					break;
				case 3:
					apresentacaoFuncionario.listarFuncionario();
					break;
				case 4:
					if (apresentacaoFuncionario.removerFuncionario()) {
						System.out.println("Removido com sucesso.");
						break;
					}
					System.out.println("N�o Removido.");
					break;
				case 5:
					menu = false;
					break;
				}
				break;
			case 2:
				System.out.println("======Menu de Clientes=======");
				System.out.println("Escolha uma op��o: ");
				System.out.println("1 - Cadastrar cliente.");
				System.out.println("2 - Alterar o cadastro de um cliente.");
				System.out.println("3 - Listar todos os cliente.");
				System.out.println("4 - Excluir um cliente.");
				System.out.println("5 - Sair do sistema.");
				int escolhaMenuClientes = input.nextInt();
				switch (escolhaMenuClientes) {
				case 1:
					if (apresentacaoCliente.cadastrarCliente()) {
						System.out.println("Salvo com sucesso.");
						break;
					} 
					System.out.println("N�o salvo.");
					break;
				case 2:
					if (apresentacaoCliente.atualizarCliente()) {
						System.out.println("Salvo com sucesso.");
						break;
					} 
					System.out.println("N�o salvo.");
					break;
				case 3:
					apresentacaoCliente.listarCliente();
					break;
				case 4:
					if (apresentacaoCliente.removerCliente()) {
						System.out.println("Removido com sucesso.");
						break;
					}
					System.out.println("N�o Removido.");
					break;
				case 5:
					menu = false;
					break;
				}
				
				break;
			case 3:
				System.out.println("======Menu de Livros=======");
				System.out.println("Escolha uma op��o: ");
				System.out.println("1 - Cadastrar Livro.");
				System.out.println("2 - Alterar o cadastro de um Livro.");
				System.out.println("3 - Listar todos os Livros.");
				System.out.println("4 - Excluir um Livro.");
				System.out.println("5 - Sair do sistema.");
				int escolhaMenuLivros = input.nextInt();
				switch (escolhaMenuLivros) {
				case 1:
					if (apresentacaoLivro.cadastrarLivro()) {
						System.out.println("Salvo com sucesso.");
						break;
					}
					System.out.println("N�o salvo.");
					break;
				case 2:
					if (apresentacaoLivro.atualizarLivro()) {
						System.out.println("Salvo com sucesso.");
						break;
					}
					System.out.println("N�o salvo.");
					break;
				case 3:
					apresentacaoLivro.listarLivros();
					break;
				case 4:
					if (apresentacaoLivro.removerLivro()) {
						System.out.println("Removido com sucesso.");
						break;
					}
					System.out.println("N�o removido.");
					break;
				case 5:
					menu = false;
					break;
				}
				break;
			case 4:
				menu = false;
			}
		}
	}
}
