
/**
 *
 * @author matheus,waschislin,julio cesar
 */
package Dados;

import Entidades.Livro;
import Entidades.Livro;
import java.util.ArrayList;
import java.util.List;

public class LivroDados extends Livro {
	
	public static ArrayList<Livro> dadosLivro = new ArrayList<Livro>();
	
	/**
	 * M�todo que adiciona um Livro ao ArrayList.
	 * @param Livro
	 * @return
	 */
	public boolean cadastrarLivro(Livro livro) {
		return dadosLivro.add(livro);
	}
	
	/**
	 * M�todo que retorna todo o ArrayList dadosLivro para ser listado.
	 * @return
	 */
	public ArrayList<Livro> listarLivros() {
		return dadosLivro;
	}
	
	/**
	 * M�todo que remove o Livro de acordo com uma posi��o passada como par�metro para o m�todo e ArrayList.
	 * @param posicao
	 * @return
	 */
	public boolean removerLivro(int posicao) {
			dadosLivro.remove(posicao);
			return true;
	}
	
	/**
	 * M�todo que atualiza um Livro de acordo com a posi��o que recebe como par�metro.
	 * @param posicao
	 * @return
	 */
	public boolean atualizarLivro(int posicao,Livro Livro) {
			dadosLivro.set(posicao, Livro);
			return true;
	}
}