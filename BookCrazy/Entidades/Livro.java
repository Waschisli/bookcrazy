/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author matheus,waschislin,julio cesar
 */
package Entidades;

public class Livro {

	private String nomeLivro;
	private String autor;
	private Long isbn;
	private String editora;
	private int numPag;

	public Livro(String nomeLivro, String autor, Long isbn, String editora, int numPag) {
		this.nomeLivro = nomeLivro;
		this.autor = autor;
		this.isbn = isbn;
		this.editora = editora;
		this.numPag = numPag;
	}

	public Livro() {
	}

	public String toString() {
		String livroNome = nomeLivro;
		String autor1 = autor;
		double isbn1 = isbn;
		String editora1 = editora;
		int numPagina = numPag;
		return "Nome do livro: " + livroNome + " Nome do autor: " + autor1 + " ISBN: " + isbn1 + " Editora: " + editora1 + " N�mero de paginas: " + numPagina;
	}

	public String getNomeLivro() {
		return nomeLivro;
	}

	public void setNomeLivro(String nomeLivro) {
		this.nomeLivro = nomeLivro;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public Long getIsbn() {
		return isbn;
	}

	public void setIsbn(Long isbn) {
		this.isbn = isbn;
	}

	public String getEditora() {
		return editora;
	}

	public void setEditora(String editora) {
		this.editora = editora;
	}

	public int getNumPag() {
		return numPag;
	}

	public void setNumPag(int numPag) {
		this.numPag = numPag;
	}

}
