/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Entidades.Cliente;
import java.util.ArrayList;
import Dados.ClienteDados;

/**
 *
 * @author matheus, J�lio Oliveira
 */
public class NegocioCliente extends Cliente {

	//Cria��o dos objetos necess�rios para serem manipulados na classe.
    ClienteDados clienteDados = new ClienteDados();
    NegocioUtils negocioUtils = new NegocioUtils();
    
	/**
	 * Construtor da classe NegocioCliente com todos os atributos.
	 * @param nome
	 * @param sobrenome
	 * @param cpf
	 * @param endereco
	 */
	public NegocioCliente(String nome,String sobrenome,String cpf,String endereco) {
		super(nome,sobrenome,cpf,endereco);
	}
	
	/**
	 * Construtor da classe NegocioCliente sem atributos.
	 */
	public NegocioCliente() {}
    
    /**
     * M�todo que cadastra o cliente, se suas informa��es forem validadas.
     * @param cliente
     * @return
     */
	public boolean cadastrarCliente(Cliente cliente) {
		if (negocioUtils.validaCpf(cliente.getCpf()) == false) {
			return false;
		} else if (negocioUtils.validaNomeSobrenome(cliente.getNome()) == true || negocioUtils.validaNomeSobrenome(cliente.getSobrenome()) == true) {
			return false;
		}
		cliente.setCpf(negocioUtils.formatarCpf(cliente.getCpf()));
		return clienteDados.cadastrarCliente(cliente);
	}
	/**
	 * M�todo que retorna o ArrayList de clientes para a classe ClienteApresenta��o.
	 * @return
	 */
	public ArrayList<Cliente> listarCliente() {
		return clienteDados.listarCliente();
	
	}
	/**
	 * M�todo que chama o m�todo remover da classe ClienteDados.
	 * @param posicao
	 * @return
	 */
	public boolean removerCliente(int posicao){
		return clienteDados.removerCliente(posicao);
	}
	/**
	 * M�todo que atualiza o cliente, se suas informa��es forem validadas.
	 * @param posicao
	 * @param cliente
	 * @return
	 */
	public boolean atualizarCliente(int posicao,NegocioCliente cliente){
		
		if (negocioUtils.validaCpf(cliente.getCpf()) == false) {
			return false;
		} else if (negocioUtils.validaNomeSobrenome(getNome()) == true || negocioUtils.validaNomeSobrenome(getSobrenome()) == true) {
			return false;
		}
		return clienteDados.atualizarCliente(posicao,cliente);
	}
}